# project data
name=dd_lang
version=0.0

# source files and objects
SRC=lex.yy.c y.tab.c $(wildcard src/*.c)
OBJ=${SRC:.c=.o}
HEADERS=y.tab.h $(wildcard headers/*.h)

# executable
${name}: ${OBJ}
	gcc ${OBJ} -o ${name}

# lexer
lex.yy.c: src/mylex.l
	lex src/mylex.l

# yacc
y.tab.c: src/mylex.y
	yacc -d src/mylex.y

y.tab.h: src/mylex.y
	yacc -d src/mylex.y

# clean
clean:
	rm -rf lex.yy.c y.tab.c y.tab.h ${OBJ} ${name}

.PHONY: clean

# recompile after header change
${OBJ}: ${HEADERS}

# compile object files
.c.o:
	gcc -c -o ${<:.c=.o} $< -I headers -I .
