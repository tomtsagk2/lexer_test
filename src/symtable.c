#include <string.h>
#include <stdio.h>
#include "symtable.h"

#define STRMAX 999 // size of lexemes array
#define SYMMAX 100 // size of symtable

char lexemes[STRMAX];
int lastchar = -1; // last used position in lexemes
struct entry symtable[SYMMAX];
int lastentry = 0; //last used position in symtable

int lookup(char s[]) {
	int p;
	for (p = lastentry; p> 0; p--) {
		if (strcmp(symtable[p].lexptr, s) == 0) {
			return p;
		}
	}
	return 0;
}

int insert(char s[], int tok) {

	int index = lookup(s);
	if (index) {
		return index;
	}

	int len;
	len = strlen(s);
	if (lastentry +1 >= SYMMAX) {
		printf("symbol table full");
		return 0;
	}

	if (lastchar +len +1 >= STRMAX) {
		printf("lexemes array full");
		return 0;
	}

	lastentry = lastentry +1;
	symtable[lastentry].token = tok;
	symtable[lastentry].lexptr = &lexemes[lastchar +1];
	lastchar = lastchar +len +1;
	strcpy(symtable[lastentry].lexptr, s);
	return lastentry;
}

int insert_data(char s[], int tok, int data) {
	int index = insert(s, tok);
	symtable[index].data = data;
}

// struct

char struct_lexemes[STRMAX];
int struct_lastchar = -1; // last used position in lexemes
struct struct_entry struct_symtable[SYMMAX];
int struct_lastentry = 0; //last used position in symtable

int struct_lookup(char s[]) {
	int p;
	for (p = struct_lastentry; p > 0; p--) {
		if (strcmp(struct_symtable[p].lexptr, s) == 0) {
			return p;
		}
	}
	return 0;
}

int struct_insert(char s[], struct entry *st) {

	int index = struct_lookup(s);
	if (index) {
		return index;
	}

	int len;
	len = strlen(s);
	if (struct_lastentry +1 >= SYMMAX) {
		printf("struct symbol table full");
		return 0;
	}

	if (struct_lastchar +len +1 >= STRMAX) {
		printf("struct lexemes array full");
		return 0;
	}

	struct_lastentry = struct_lastentry +1;
	struct_symtable[struct_lastentry].st = st;
	struct_symtable[struct_lastentry].lexptr = &struct_lexemes[struct_lastchar +1];
	struct_lastchar = struct_lastchar +len +1;
	strcpy(struct_symtable[struct_lastentry].lexptr, s);
	return struct_lastentry;

}
