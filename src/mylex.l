%{
#include <stdio.h>
#include "y.tab.h"
#include "symtable.h"
%}
%%

int			yylval = INTEGER; return INTEGER;
float			yylval = FLOAT; return FLOAT;
struct			yylval = STRUCT; return STRUCT;

world			return WORLD;
[a-zA-Z]+		yylval = insert(yytext, UNDEFINED); return ID;
[0-9]+			yylval = atoi(yytext); return NUMBER;
\{			return BRACEA;
\}			return BRACEB;
;			return SEMICOLON;
=			return '=';
\+			return '+';
\-			return '-';
\*			return '*';
\/			return '/';
\(			return '(';
\)			return ')';
,			return ',';
[ \t\n]+
\.			return DOT;
%%
