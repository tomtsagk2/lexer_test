%{
#include <stdio.h>
#include <string.h>
#include "symtable.h"
#include "y.tab.h"
#include "ast_node.h"
#include "translation.h"
 
void yyerror(const char *str)
{
        fprintf(stderr,"error: %s\n",str);
}
 
int yywrap()
{
        return 1;
} 

struct entry struct_sprite[] = {
	"x", INTEGER, 0,
	0, 0, 0,
};

struct struct_entry basic_structs[] = {
	"sprite", struct_sprite,
	0, 0,
};
int last_struct = 0;

// Game Abstract Syntax Tree node
struct ast_node *game_node;

// Load initial entries in the symbol table
void init() {
	ast_table_lastentry = -1;
	/*
	struct struct_entry *se;
	int i = 1;
	for (se = basic_structs; se->st; se++) {
		insert_data(se->lexptr, STRUCT, i);
		struct_insert(se->lexptr, se->st);
		i++;
	}
	*/
}
  
int main()
{
	// Initialise game AST
	game_node = ast_create(GAME, 0);

	/*
	struct ast_node *n2 = ast_create(4, 4);
	struct ast_node *n3 = ast_create(5, 4);
	struct ast_node *n4 = ast_create(6, 4);
	ast_child_add(game_node, n2);
	ast_child_add(n3, n4);
	ast_child_add(game_node, n3);
	*/

	init();
        yyparse();

	ast_print(game_node);
	translate(game_node);

	for (int i = 0; i <= ast_table_lastentry; i++) {
		ast_print(ast_table[i]);
	}

	/*
	printf("printing the symbol table\n");
	for (int i = 0; i <= lastentry; i++) {
		printf("lex: %s | token: %d | data: %d\n", symtable[i].lexptr, symtable[i].token, symtable[i].data);
	}
	printf("printing the struct symbol table\n");
	for (int i = 0; i <= struct_lastentry; i++) {
		printf("lex: %s | token: %d\n", struct_symtable[i].lexptr, struct_symtable[i].st);
	}
	*/
	return 0;
} 

%}

%token GAME WORLD DEFINE

%token ADDITION SUBSTRACTION MULTIPLICATION DIVISION ASSIGNMENT

%token ID BRACEA BRACEB SEMICOLON VAR_TYPE DOT STRUCT

%token NUMBER

%token UNDEFINED INTEGER FLOAT

%%

/* Find worlds */
worlds:
      	| worlds world
	;

/* World syntax */
world:
      	WORLD ID BRACEA statements BRACEB
	{
		// Add world to game AST
		struct ast_node *world_node = ast_create(WORLD, $2);

		// Add statements into the world
		for (int i = 0; i <= ast_table_lastentry; i++) {
			if (ast_table[i]) {
				ast_child_add(world_node, ast_table[i]);
			}
		}
		// Add world into the game
		ast_child_add(game_node, world_node);

		// Reset statements
		ast_table_lastentry = -1;
	}
	;

/* Find statements inside worlds */
statements:
	| statements statement
	;

/* statement syntax
 ** declare variable
 ** assign expression to variable
 */
statement:
	/* delcaration statement */
	var_type ID SEMICOLON
	{
		// Undefined variable
		if (symtable[$2].token == UNDEFINED) {

			// Give it type
			symtable[$2].token = $1;

			// Create an abstract node for the definition and pass its index on the table
			struct ast_node *n = ast_create(DEFINE, $2);
			ast_table_lastentry++;
			ast_table[ast_table_lastentry] = n;
			//$$ = ast_table_lastentry;

			// If struct, get the struct index
			/*
			if ($1 == STRUCT) {
				symtable[$2].data = last_struct;
				//printf("id: %s | data: %d\n", symtable[$2].lexptr, symtable[$2].data);
			}
			*/
		}
		// Already defined
		else {
			printf("identifier %s has already been initialised\n", symtable[$2].lexptr);
		}
	}
	|
	/* assignement */
	ID '=' expressions SEMICOLON
	{
		struct ast_node *n = ast_table[ast_table_lastentry];

		if (symtable[$1].token != UNDEFINED) {
			//printf("assignment: %s =\n", symtable[$1].lexptr);
			//ast_print(n);

			struct ast_node *nn = ast_create(ASSIGNMENT, 0);
			struct ast_node *n2 = ast_create(ID, $1);
			ast_child_add(nn, n2);
			ast_child_add(nn, n);
			// Do not increment last entry, as it is replacing the last expression
			ast_table[ast_table_lastentry] = nn;
		}
		else {
			printf("undefined variable: %s\n", symtable[$1].lexptr);
		}
	}
	|
	var_type ID '(' arguments ')' BRACEA arguments BRACEB SEMICOLON
	{
		printf("function declaration\n");
	}
	|
	ID DOT ID SEMICOLON
	{
		/*
		printf("getting data from struct identifier\n");
		if (symtable[$1].token == STRUCT) {
			printf("%s is a struct\n", symtable[$1].lexptr);

			int struct_index = symtable[$1].data;
			printf("its struct is called %s\n", struct_symtable[struct_index].lexptr);

		}
		else {
			printf("%s is NOT a struct\n", symtable[$1].lexptr);
		}
		*/
	}
	;

expressions:
	| expression expressions
	;

expression:
	term '+' expression
	{
		//printf("%d + %d\n", $1, $3);
		// Get numbers
		struct ast_node *n1 = ast_table[ast_table_lastentry-1];
		struct ast_node *n2 = ast_table[ast_table_lastentry];

		// Create new node for addition
		struct ast_node *n = ast_create(ADDITION, 0);
		ast_child_add(n, n1);
		ast_child_add(n, n2);
		ast_table[ast_table_lastentry-1] = n;
		ast_table_lastentry--;
	}
	|	
	term '-' expression
	{
		//printf("%d - %d\n", $1, $3);
		// Get numbers
		struct ast_node *n1 = ast_table[ast_table_lastentry-1];
		struct ast_node *n2 = ast_table[ast_table_lastentry];

		// Create new node for addition
		struct ast_node *n = ast_create(SUBSTRACTION, 0);
		ast_child_add(n, n1);
		ast_child_add(n, n2);
		ast_table[ast_table_lastentry-1] = n;
		ast_table_lastentry--;
	}
	|
	term 
	;

term:
	factor '*' term
	{
		//printf("%d * %d\n", $1, $3);
		// Get numbers
		struct ast_node *n1 = ast_table[ast_table_lastentry-1];
		struct ast_node *n2 = ast_table[ast_table_lastentry];

		// Create new node for addition
		struct ast_node *n = ast_create(MULTIPLICATION, 0);
		ast_child_add(n, n1);
		ast_child_add(n, n2);
		ast_table[ast_table_lastentry-1] = n;
		ast_table_lastentry--;
	}
	|
	factor '/' term
	{
		//printf("%d / %d\n", $1, $3);
		// Get numbers
		struct ast_node *n1 = ast_table[ast_table_lastentry-1];
		struct ast_node *n2 = ast_table[ast_table_lastentry];

		// Create new node for addition
		struct ast_node *n = ast_create(DIVISION, 0);
		ast_child_add(n, n1);
		ast_child_add(n, n2);
		ast_table[ast_table_lastentry-1] = n;
		ast_table_lastentry--;
	}
	|
	factor
	;

factor:
	NUMBER
	{
		//printf("number: %d\n", $1);
		struct ast_node *n = ast_create(NUMBER, $1);
		ast_table_lastentry++;
		ast_table[ast_table_lastentry] = n;
	}
	|
	ID
	{
		//printf("number: %d\n", $1);
		struct ast_node *n = ast_create(ID, $1);
		ast_table_lastentry++;
		ast_table[ast_table_lastentry] = n;
	}
	;

      

var_type:
	INTEGER
	|
	FLOAT
	|
	STRUCT ID {last_struct = symtable[$2].data;}
	;

arguments:
	|
	argument arguments
	{
		printf("argument read\n");
	}
	;

argument:
	var_type ID
	|
	var_type ID ','

