#ifndef _SYMTABLE_H_
#define _SYMTABLE_H_

// Normal symbol table
int lookup(char s[]);
int insert(char s[], int tok);
int insert_data(char s[], int tok, int data);
int lastentry;

struct entry {
	char *lexptr;
	int token;
	int data;
};
extern struct entry symtable[];

// Struct symbol table
int struct_lookup(char s[]);

struct struct_entry {
	char *lexptr;
	struct entry *st;
};
extern struct struct_entry struct_symtable[];

int struct_lookup(char s[]);
int struct_insert(char s[], struct entry *st);
int struct_lastentry;

#endif
