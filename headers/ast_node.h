#ifndef _PGMPREFIX_AST_NODE_H_
#define _PGMPREFIX_AST_NODE_H_

#include "dd_dynamic_array.h"

// Struct for a single node
struct ast_node {
	int token;
	struct dd_dynamic_array children;
	int value;
};

// Actions
struct ast_node *ast_create(int token, int value);
void ast_child_add(struct ast_node *parent, struct ast_node *child);
void ast_delete(struct ast_node *node);

// Debug - Print node tree
void ast_print(struct ast_node *node);

// AST table
struct ast_node* ast_table[100];
int ast_table_lastentry;

#endif
