#ifndef _PRGMPREFIX_TRANSLATION_H_
#define _PRGMPREFIX_TRANSLATION_H_

#include "symtable.h"
#include "ast_node.h"

void translate(struct ast_node *node);

#endif
